package com.demo.kenyo.config;


import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.demo.kenyo.controller.PersonaController;




@ApplicationPath("rest")
public class DemoApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(PersonaController.class);
		return classes;
	}
}