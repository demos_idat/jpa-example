package com.demo.kenyo.service.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import com.demo.kenyo.dao.IPersonaDao;
import com.demo.kenyo.domain.Persona;
import com.demo.kenyo.domain.Telefono;
import com.demo.kenyo.exception.MiException;
import com.demo.kenyo.service.IPersonaService;

@Named
public class PersonaServiceImpl implements IPersonaService{
	
	@EJB
	private IPersonaDao dao;

	public Persona registrar(Persona t) throws MiException {
		try {
			for (Telefono telefono : t.getTelefonos()) {
				telefono.setPersona(t);
			}
			t = dao.registrar(t);
		} catch (Exception e) {
			throw new MiException(e.getMessage());
		}
		return t;
	}

	public Persona modficar(Persona t) throws MiException {
		try {
			for (Telefono telefono : t.getTelefonos()) {
				telefono.setPersona(t);
			}
			t = dao.modificar(t);
		} catch (Exception e) {
			throw new MiException(e.getMessage());
		}
		return t;
	}

	public Persona buscarID(int id) throws MiException {
		Persona persona = dao.buscarPorId(id);
		return persona;
	}

	public List<Persona> listar() throws MiException {
		return dao.listar();
	}

	public void eliminar(int id) throws MiException {
		dao.eliminar(id);
	}


	

}
