package com.demo.kenyo.service;

import java.util.List;

import com.demo.kenyo.exception.MiException;

public interface IService<T> {
	
	T registrar(T t) throws MiException;
	
	T modficar(T t) throws MiException;

	T buscarID(int id) throws MiException;

	List<T> listar() throws MiException;

	void eliminar(int id) throws MiException;

}
