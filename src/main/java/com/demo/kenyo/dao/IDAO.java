package com.demo.kenyo.dao;

import java.util.List;


public interface IDAO<T> {
	
	T registrar(T t);
	T modificar(T t) ;
	List<T> listar() ;
	T buscarPorId(int id) ;
	void eliminar(int id) ;

}
