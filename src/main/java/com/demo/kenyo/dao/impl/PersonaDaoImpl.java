package com.demo.kenyo.dao.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.demo.kenyo.dao.IPersonaDao;
import com.demo.kenyo.domain.Persona;

@Stateless
public class PersonaDaoImpl implements IPersonaDao{
	
	@PersistenceContext(unitName="DirectorioPU")
	private EntityManager em;

	public Persona registrar(Persona t)  {
		em.persist(t);
		em.flush();
		return t;
	}

	public Persona modificar(Persona t)  {
		em.merge(t);
		em.flush();
		return t;
	}

	public List<Persona> listar()  {
		List<Persona> lista;
		Query query = em.createQuery("from Persona p");
		lista = query.getResultList();
		return lista;
	}

	public Persona buscarPorId(int id)  {
		List<Persona> lista;
		Query query = em.createQuery("from Persona p where p.id = ?1");
		query.setParameter(1, id);
		lista = query.getResultList();
		return lista != null && !lista.isEmpty() ? lista.get(0) : null;
	}

	public void eliminar(int id)  {
		Persona persona = buscarPorId(id);
		em.remove(persona);
	}



}
